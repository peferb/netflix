FROM nginx:stable-alpine

# Put all in /netflix cause its proxy passed on ferb.se/netflix
COPY /dist /usr/share/nginx/html/netflix