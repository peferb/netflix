# netflix

Filterable list of Netflix genres not shown by Netflix themselves.

### Run project
npm install
gulp build
deploy /dist

### Stuff to fix
// TODO Fix gulp JS concat breaking JS
// TODO Make genre 'code' available for user in a nice way. Add to SEO texts: ~ 'And the codes! All easy to filter!'
// TODO 'Compile' CSS, or change to SCSS
// TODO SEO: Make parent categories h2, make children ul
// TODO SEO: micro data etc
// TODO Switch question mark to close mark on top bar button
// TODO Center
// TODO Remove bootstrap, or make to Vue or React, or something