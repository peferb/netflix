/**
 * Category card showing parent category as header and child categories as list.
 *
 * Can search parent and child category titles. Returns booleans.
 *
 * Filter: Can hide/show whole card or just specific child categories depending och text/title match.
 */
export default class CategoryCard {

    /** string **/
    _parentCategoryTitle = '';

    /** [{title:string, view:$element}] **/
    _childCategories = [];

    /** $element **/
    _$cardRoot;

    /**
     * @param $cardRoot
     */
    constructor($cardRoot) {
        this._$cardRoot = $cardRoot;

        // Cache parent category title (lower case) to speed up filtering
        this._parentCategoryTitle = $cardRoot.find('.category-parent')
            .text()
            .toLowerCase();

        // Cache views with lower case title to speed up filtering
        this._childCategories = $cardRoot.find('.category--child').map((index, item) => {
            return {
                title: $(item).text().toLowerCase(),
                $view: $(item)
            }
        });
    }

    /**
     * Does the category parent contain this text?
     * 
     * @param text
     * @returns {boolean}
     */
    parentTitleContains = text => this._parentCategoryTitle.includes(text);

    /**
     * Does any of the child categories contain this text?
     * 
     * @param text
     * @returns {boolean}
     */
    anyChildTitleContains = text => {
        for (let i = 0; i < this._childCategories.length; i++) {
            if (this._childCategories[i].title.includes(text)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Only show child categories matching this text!
     * 
     * @param text
     */
    filterChildCategoriesOn = text => {
        for (let i = 0; i < this._childCategories.length; i++) {
            this._childCategories[i].title.includes(text)
                ? this._childCategories[i].$view.show()
                : this._childCategories[i].$view.hide();
        }
    };

    /**
     * Hide the whole card!
     */
    hideCard = () => {
        this._$cardRoot.hide();
    }

    /**
     * Show card and all child categories!
     */
    showAll = () => {
        this._$cardRoot.show();
        for (let i = 0; i < this._childCategories.length; i++) {
            this._childCategories[i].$view.show();
        }
    };
}