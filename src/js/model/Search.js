import CategoryCard from "./CategoryCard.js";

export default class Search {

    _elements = {};
    _cards= [];

    constructor($root) {
        this._elements.$root = $root;
        this._elements.$inputContainer = $root.find('.input__container');
        this._elements.$inputField = $root.find('.input__field');
        this._elements.$inputClear = $root.find('.input__clear');
        this._elements.$categoryCards = $root.find('.card');
        this._elements.$noMatch = $root.find('.no-match');
        this._elements.$noMatchTerm = $root.find('.no-match__term');

        $.each(this._elements.$categoryCards, (key, cardElement) => {
            this._cards.push(new CategoryCard($(cardElement)))
        });

        this._elements.$inputField.focus();
        this._bindEvents();
    }

    _bindEvents = () => {
        this._elements.$inputField.on('keyup', () => this._handleInput(this._elements.$inputField.val()));
        this._elements.$inputClear.on('click', this._resetFiltering);
    }

    _handleInput = input => input ? this._filterLinks(input) : this._resetFiltering();

    _resetFiltering = () => {
        $.each(this._cards, (key, card) => card.showAll());
        this._elements.$inputField.val('');
        this._elements.$inputClear.hide();
        this._elements.$noMatch.hide();
    };

    _filterLinks = (text) => {
        const textLow = text.toLowerCase();

        // Hide clear button if there is no text to delete, show if there is
        text.length > 0 ? this._elements.$inputClear.show() : this._elements.$inputClear.hide();

        // Do actual filtering
        let cardsHiddenCount = 0;
        $.each(this._cards, (key, card) => {
            let parentMatch = card.parentTitleContains(textLow);
            if (parentMatch) {
                card.showAll();
                return;
            }

            let childMatch = card.anyChildTitleContains(textLow);
            card.showAll();
            if (childMatch) {
                card.filterChildCategoriesOn(textLow)
                return;
            }

            card.hideCard();
            cardsHiddenCount++;
        });

        if (cardsHiddenCount === this._cards.length)  {
            this._elements.$noMatchTerm.text(text);
            this._elements.$noMatch.show();
        } else  {
            this._elements.$noMatchTerm.text('');
            this._elements.$noMatch.hide();
        }
    }
}