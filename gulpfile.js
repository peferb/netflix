const { series, parallel } = require('gulp');
const { src, dest } = require('gulp');
const babel = require('gulp-babel');
const terser = require('gulp-terser');
const concat = require('gulp-concat');
const del = require('del');

const clean = () => del(['dist']);

const index = () => src('src/index.*').pipe(dest('dist/'))

const html = () => src('src/views/**/*.*').pipe(dest('dist/views/'))

const css = () => src('src/css/**/*.css').pipe(dest('dist/css/'))

const img = () => src('src/img/**/*.*').pipe(dest('dist/img/'))

const js = () => src('src/js/**/*.js')
        .pipe(babel())
        .pipe(terser())
        // .pipe(concat('app.js'))
        .pipe(dest('dist/js/'))

exports.img = img
exports.css = css
exports.clean = clean
exports.views = parallel(html, index)
exports.build = series(clean, parallel(js, css, img, index, html))
exports.default = cb => console.log('\nNope, use defined tasks!\n') & cb();